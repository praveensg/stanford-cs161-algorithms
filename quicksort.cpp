#include <iostream>
#include <vector>
#include <math.h>

std::pair<std::vector<int>, unsigned> sort(std::vector<int>, int);

int main(){
  std::vector<int> input;
  int value=0, size = 0, inversionCount = 0;
  while(std::cin >> value)
    input.push_back(value);
  input.shrink_to_fit();
  size = input.capacity();
  std::pair<std::vector<int>, unsigned> sorted;
  sorted = sort(input, size);
  std::cout << "Total number of inversions is : " << sorted.second << std::endl;
  return 0;
}

std::pair<std::vector<int>, unsigned> sort(std::vector<int> array, int size){
  std::vector<int> split1(size/2), split2(size-size/2);
  unsigned splitcount1=0, splitcount2=0, count = 0;
  if(size == 1)
    return std::make_pair(array, 0);
  else{
    for(int i = 0; i < size; ++i){
      if(i < size/2)
        split1[i] = array[i];
      else
        split2[i-size/2] = array[i];
    }
  }
  std::pair<std::vector<int>, unsigned> sorted1 = sort(split1, size/2);
  std::pair<std::vector<int>, unsigned> sorted2 = sort(split2, size-size/2);
  std::vector<int> sorted(size);
  
  //Merge
  int x = 0, y = 0;
  for(int i = 0; i < size; ++i){
    if(x >= size/2){
      sorted[i] = sorted2.first[y];
      y++;
    }
    else if(y >= size-size/2){
      sorted[i] = sorted1.first[x];
      x++;
    }
    else{
      if(sorted1.first[x] < sorted2.first[y]){
        sorted[i] = sorted1.first[x];
        x++;
      }
      else{
        sorted[i] = sorted2.first[y];
        y++;
        count += (size/2-x);
      }
    }
  }
  count = count + sorted1.second + sorted2.second;
  return std::make_pair(sorted, count);
}


