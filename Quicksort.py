""" 
Quicksort using python

"""

import fileinput
import random
import sys

#========================
# Finding median of three values
#========================

def findMedian(x, y, z):
    return ((x+y+z) - min(x, y, z) - max(x, y, z))

# Left most element of the list is returned as pivot

def pivotLeft(mylist, left, right):
    pivotValue = mylist[left]
    return pivotValue, mylist.index(pivotValue)

# Right most element is returned as pivot element

def pivotRight(mylist, left, right):
    pivotValue = mylist[right]
    return pivotValue, mylist.index(pivotValue)

# Median of left, right and middle elements is pivot

def pivotMedian(mylist, left, right):
    numElements = right-left+1
    small = mylist[left]
    large = mylist[right]
    if(numElements%2 == 0):
        middle = mylist[left + (numElements/2) -1]
    else:
        middle = mylist[int(left+ numElements/2)]
    median = findMedian(small, middle, large)
    return median, mylist.index(median)

#===========================
# Sorting routine
#===========================
# First, a pivot is found and it is swapped with the first element of the array.
# i and j point to second element. For loop from second element to last element: if a value lower than pivot is found, swap with value in i-th position and increment i.
# Finally, swap the first element with value in position i-1
# Recursively call this routine on array before and after the pivot(excluding)

def partition(mylist, left, right):
    if(left == right):
        return 0
    pivotVal, pivotIndex = pivotMedian(mylist, left, right) # Use pivotLeft or pivotRight or pivotMedian functions
    temp = mylist[pivotIndex]
    mylist[pivotIndex] = mylist[left]
    mylist[left] = temp
    i = left+1
    for j in range(left+1, right+1):
        if mylist[j] < pivotVal:
            temp = mylist[j]
            mylist[j] =  mylist[i]
            mylist[i] = temp
            i+=1
    temp = mylist[left]
    mylist[left] = mylist[i-1]
    mylist[i-1]=temp
    sum1 = partition(mylist, left, max(i-2, left))
    sum2 = partition(mylist, min(i, right), right)
    return sum1+sum2+(right-left)


# Main routine

array = []

sortfile = open("QuickSort.txt")

# Import the file
for line in sortfile:
    array.append(int(line))

# Sorting function call
numComp = partition(array, 0, len(array)-1)
print "The number of comparisons is", numComp

