""" Random contraction algorithm using python to find the minimum number of cuts in the graph """

import random
import copy

def mincut(graph, vertex1, vertex2):
    maxValue = max(vertex1, vertex2)
    maxNeighbors = graph.pop(maxValue)
    minValue = min(vertex1, vertex2)
    minNeighbors = graph[minValue]
    for i in graph:
        if i == minValue:
            j = 0
            k = len(graph[i])
            while j < k:
                if graph[i][j] == maxValue:
                    graph[i].pop(j)
                    k -= 1
                else:
                    j += 1
        else:
            for j in range(0, len(graph[i])):
                if graph[i][j] == maxValue:
                    graph[i][j] = minValue
    for i in maxNeighbors:
        if i != minValue:
            graph[minValue].append(i)

# Read the graph using array
edgeList = open("kargerMinCut.txt")
newedge = dict()
for line in edgeList:
    edges = line.split()
    newedge[int(edges[0])] = [int(edges[n]) for n in range(1, len(edges))]

smallestMinCut = 0
tempedge = copy.deepcopy(newedge)

# Run the algorithm n times to find the smallest mincut.
for i in range(0, 20):
    while(len(newedge) > 2):
        vertex1 = random.sample(newedge,1)[0]
        vertex2 = random.sample(newedge[vertex1],1)[0]
        mincut(newedge, vertex1, vertex2)
    currentMinCut = len(newedge[random.sample(newedge,1)[0]])
    if smallestMinCut == 0:
        smallestMinCut = currentMinCut
    elif currentMinCut < smallestMinCut:
        smallestMinCut = currentMinCut
    newedge = copy.deepcopy(tempedge)
print "The smallest min cut is", smallestMinCut
